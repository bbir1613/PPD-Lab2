package domain;

import operatos.Operator;

import java.lang.reflect.Array;

public class SyncMatrix<T> implements Matrix {
    private final Class<T> clazz;
    private int n;
    private int m;
    private T[][] matrix;

    public SyncMatrix(int n, int m, Class<T> clazz) {
        this.n = n;
        this.m = m;
        this.clazz = clazz;
        this.matrix = (T[][]) Array.newInstance(this.clazz, this.n, this.m);//init matrix
    }

    public int getN() {
        return n;
    }

    public int getM() {
        return m;
    }

    protected void setN(int n) {
        this.n = n;
    }

    protected void setM(int m) {
        this.m = m;
    }

    protected Class<T> getClazz() {
        return this.clazz;
    }

    protected T[][] getMatrix() {
        return this.matrix;
    }

    protected void setMatrix(T[][] matrix) {
        this.matrix = matrix;
    }

    public void addElement(int row, int column, T element) {
        this.matrix[row][column] = element;
    }

    public T getElement(int row, int column) {
        return this.matrix[row][column];
    }

    /**
     * @param matrix second matrix
     * @param op     defines what sum between elements means (a+b)
     * @return synchronous add  two matrix n*m & n*m,throws runtime exception if the condition is not respected
     */
    public SyncMatrix<T> add(SyncMatrix<T> matrix, Operator<T> op) {
        if (this.n != matrix.getN() || this.m != matrix.getM()) {
            throw new RuntimeException("Invalid matrix");
        }
        for (int row = 0; row < this.n; row++) {
            for (int column = 0; column < this.m; column++) {
                T element = op.apply(getElement(row, column), matrix.getElement(row, column));
                addElement(row, column, element);
            }
        }
        return this;
    }

    public T[] getRow(int row) {
        return this.matrix[row];
    }

    /**
     * @param column index
     * @return the column
     */
    public T[] getColumn(int column) {
        T[] result = (T[]) Array.newInstance(this.clazz, this.n);
        for (int i = 0; i < this.n; i++) {
            result[i] = this.matrix[i][column];
        }
        return result;
    }

    protected void setRow(int index, T[] row) {
        this.matrix[index] = row;
    }

    /**
     * @param row        from first matrix
     * @param column     from second matrix
     * @param addOp      defines what sum between elements is ( a1*b1+a1*b2 ... )
     * @param multiplyOp defines what multiply between elements means ( a1 * b1)
     * @return synchronous product multiplies row & column form a matrix
     */
    private T multiplyArray(T[] row, T[] column, Operator<T> addOp, Operator<T> multiplyOp) {
        T result = null;
        for (int i = 0; i < row.length; i++) {
            result = addOp.apply(result, multiplyOp.apply(row[i], column[i]));
        }
        return result;
    }

    /**
     * @param matrix second matrix
     * @param addOp      defines what sum between elements is ( a1*b1+a1*b2 ... )
     * @param multiplyOp defines what multiply between elements means ( a1 * b1)
     * @return synchronous multiply two matrix nxk,kxm , throws runtime exception if the condition is not respected
     */
    public SyncMatrix<T> multiply(SyncMatrix<T> matrix, Operator<T> addOp, Operator<T> multiplyOp) {
        if (this.m != matrix.getN()) {
            throw new RuntimeException("Invalid matrix");
        }
        T[][] result = (T[][]) Array.newInstance(this.clazz, this.n, matrix.getM());
        for (int row = 0; row < this.n; row++) {
            for (int column = 0; column < matrix.getM(); column++) {
                result[row][column] = multiplyArray(getRow(row), matrix.getColumn(column), addOp, multiplyOp);
            }
        }
        this.m = matrix.getM();
        this.matrix = result;
//        SyncMatrix<T> mResult = new SyncMatrix<>(this.n, matrix.getM(), this.clazz);
//        mResult.setMatrix(result);
//        return mResult;
        return this;
    }

    @Override
    public String toString() {
        final char SEPARATOR = ' ';
        StringBuilder stringBuilder = new StringBuilder();
        for (int row = 0; row < this.n; row++) {
            for (int column = 0; column < this.m; column++) {
                stringBuilder.append(getElement(row, column));
                stringBuilder.append(SEPARATOR);
            }
            stringBuilder.append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

}
