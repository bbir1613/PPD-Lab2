package domain;

import operations.AddMatrix;
import operations.MultiplyMatrix;
import operatos.Operator;
import util.TimeLogger;

import java.util.ArrayList;
import java.util.List;

public class ParallelMatrix<T> extends SyncMatrix<T> implements Matrix {

    private final int nrOfThreads;

    public ParallelMatrix(int n, int m, Class<T> clazz, int nrOfThreads) {
        super(n, m, clazz);
        this.nrOfThreads = nrOfThreads;
    }

    /**
     * @param matrix
     * @param addOp      defines what sum between elements is ( a1*b1+a1*b2 ... )
     * @param multiplyOp defines what multiply between elements means ( a1 * b1)
     * @return asynchronous multiply two matrix nxk,kxm , throws runtime exception if the condition is not respected
     */
    public ParallelMatrix<T> asyncMultiplyMatrix(SyncMatrix<T> matrix, Operator<T> addOp, Operator<T> multiplyOp) {
        if (this.getM() != matrix.getN()) {
            throw new RuntimeException("Invalid matrix");
        }
        List<Thread> threadList = new ArrayList<>();
        SyncMatrix<T> result = new SyncMatrix<>(this.getN(), matrix.getM(), this.getClazz());
        int nrOfElements = this.getN() / nrOfThreads;
        int remainder = this.getN() % nrOfThreads;
        int lines = 0;
        while (lines < this.getN()) {
            int copyNrOfElements = nrOfElements;
            if (remainder > 0) {
                copyNrOfElements++;
            }
            SyncMatrix<T> syncMatrix = new SyncMatrix<>(copyNrOfElements, this.getM(), this.getClazz());
            int line = lines;
            int row = 0;
            while (copyNrOfElements > 0) {
                for (int j = 0; j < this.getM(); j++) {
                    syncMatrix.setRow(row, this.getRow(line));
                }
                row++;
                line++;
                copyNrOfElements--;
            }
            threadList.add(new Thread(new MultiplyMatrix<>(syncMatrix, matrix, result, addOp, multiplyOp, lines)));
            lines += nrOfElements;
            if (remainder > 0) {
                lines++;
                remainder -= 1;
            }
        }
        TimeLogger timeLogger = new TimeLogger();
        timeLogger.start();
        threadList.forEach(Thread::start);
        for (Thread th : threadList) {
            try {
                th.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.setMatrix(result.getMatrix());
        this.setN(result.getN());
        this.setM(result.getM());
        timeLogger.end();
        System.out.println(String.format("Global time : %s", timeLogger));
        return this;
    }

    /**
     * @param matrix
     * @param addOp  defines what sum between elements means (a+b)
     * @return asynchronous add two matrix nxm,nxm , throw runtime exception if the condition is not respected
     */
    public ParallelMatrix<T> asyncAddMatrix(SyncMatrix<T> matrix, Operator<T> addOp) {
        if (getN() != matrix.getN() || getM() != matrix.getM()) {
            throw new RuntimeException("Invalid matrix");
        }
        List<Thread> threadList = new ArrayList<>();
        SyncMatrix<T> result = new SyncMatrix<>(getN(), getM(), this.getClazz());
        int nrOfElements = this.getN() / nrOfThreads;
        int remainder = this.getN() % nrOfThreads;
        int lines = 0;
        while (lines < this.getN()) {
            int copyNrOfElements = nrOfElements;
            if (remainder > 0) {
                copyNrOfElements++;
            }
            SyncMatrix<T> syncMatrix1 = new SyncMatrix<>(copyNrOfElements, this.getM(), this.getClazz());
            SyncMatrix<T> syncMatrix2 = new SyncMatrix<>(copyNrOfElements, this.getM(), this.getClazz());
            int line = lines;
            int row = 0;
            while (copyNrOfElements > 0) {
                for (int j = 0; j < this.getM(); j++) {
                    syncMatrix1.setRow(row, this.getRow(line));
                    syncMatrix2.setRow(row, matrix.getRow(line));
                }
                row++;
                line++;
                copyNrOfElements--;
            }
            threadList.add(new Thread(new AddMatrix<>(syncMatrix1, syncMatrix2, result, addOp, lines)));
            lines += nrOfElements;
            if (remainder > 0) {
                lines++;
                remainder -= 1;
            }
        }
        TimeLogger timeLogger = new TimeLogger();
        timeLogger.start();
        threadList.forEach(Thread::start);
        for (Thread th : threadList) {
            try {
                th.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.setMatrix(result.getMatrix());
        this.setN(result.getN());
        this.setM(result.getM());
        timeLogger.end();
        System.out.println(String.format("Global time : %s", timeLogger));
        return this;
    }
}
