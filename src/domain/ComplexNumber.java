package domain;

public class ComplexNumber {
    double a;
    double b;

    public ComplexNumber(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public ComplexNumber(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public double getReal() {
        return a;
    }

    public double getImaginary() {
        return b;
    }

    public ComplexNumber multiply(ComplexNumber complexNumber) {
        double real = (getReal() + complexNumber.getReal()) - (getImaginary() * complexNumber.getImaginary());
        double imaginary = (getReal() * complexNumber.getImaginary() + getImaginary() * complexNumber.getReal());
        return new ComplexNumber(real, imaginary);
    }

    public ComplexNumber add(ComplexNumber complexNumber) {
        double real = getReal() + complexNumber.getReal();
        double imaginary = getImaginary() + complexNumber.getImaginary();
        return new ComplexNumber(real, imaginary);
    }

    private String getNumber(double number) {
        if (number == (long) number) {
            return String.format("%d", (long) number);
        }
        return String.format("%.1f", number);
    }

    @Override
    public String toString() {
        return new StringBuilder().append(getNumber(this.a)).append(this.b > 0 ? '+' : ' ').append(getNumber(this.b)).append('i').toString();
    }
}
