import domain.ComplexNumber;
import domain.ParallelMatrix;
import domain.SyncMatrix;
import util.FileSystem;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        System.out.println(" Preparing tests files \n please wait");
        FileSystem fileSystem = new FileSystem("a");
        fileSystem.writeToFile(500, 500);
        fileSystem = new FileSystem("b");
        fileSystem.writeToFile(500, 500);
        BufferedReader br = null;
        br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        int nrOfThreads;
        while (true) {
            try {
                System.out.println("Number of threads, x for exit :");
                input = br.readLine();
                if (input.equals("x") || input.equals("X")) {
                    break;
                } else {
                    nrOfThreads = new Integer(input);
                    ParallelMatrix<ComplexNumber> parallelMatrix = new ParallelMatrix<>(500, 500, ComplexNumber.class, nrOfThreads);
                    fileSystem = new FileSystem("a");
                    parallelMatrix = fileSystem.readFromFile(parallelMatrix, what -> {
                        String[] nr = what.split("\\.");
                        return new ComplexNumber(Double.parseDouble(nr[0]), Double.parseDouble(nr[1]));
                    });
                    SyncMatrix<ComplexNumber> syncMatrix = new SyncMatrix<>(500, 500, ComplexNumber.class);
                    fileSystem = new FileSystem("b");
                    syncMatrix = fileSystem.readFromFile(syncMatrix, what -> {
                        String[] nr = what.split("\\.");
                        return new ComplexNumber(Double.parseDouble(nr[0]), Double.parseDouble(nr[1]));
                    });
                    System.out.println("Sum : ");
                    parallelMatrix.asyncAddMatrix(syncMatrix, ComplexNumber::add);
                    System.out.println("Multiply : ");
                    parallelMatrix.asyncMultiplyMatrix(syncMatrix, (x, y) -> x == null ? y : x.add(y), ComplexNumber::multiply);
                }
            } catch (Exception e) {
                System.out.println(" An error occurred \n");
                e.printStackTrace();
            }
        }
    }
}
