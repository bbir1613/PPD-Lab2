package operations;

import domain.Matrix;
import domain.SyncMatrix;
import operatos.Operator;
import util.TimeLogger;

public class MultiplyMatrix<T> implements Runnable {

    private final SyncMatrix<T> matrix1;
    private final SyncMatrix<T> matrix2;
    private final SyncMatrix<T> result;
    private final Operator<T> addOp;
    private final Operator<T> multiplyOp;
    private final int row;
    private final TimeLogger timeLogger = new TimeLogger();

    /**
     * @param matrix1    first matrix
     * @param matrix2    second matrix
     * @param result     the result matrix
     * @param addOp      defines what sum between elements is ( a1*b1+a1*b2 ... )
     * @param multiplyOp defines what multiply between elements means ( a1 * b1)
     * @param row        what row need to be update
     */
    public MultiplyMatrix(SyncMatrix<T> matrix1, SyncMatrix<T> matrix2, SyncMatrix<T> result, Operator<T> addOp, Operator<T> multiplyOp, int row) {
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.result = result;
        this.addOp = addOp;
        this.multiplyOp = multiplyOp;
        this.row = row;
    }

    @Override
    public void run() {
        timeLogger.start();
        matrix1.multiply(matrix2, addOp, multiplyOp);
        int copyRow = this.row;
        for (int i = 0; i < matrix1.getN(); i++) {
            for (int j = 0; j < matrix1.getM(); j++) {
                result.addElement(copyRow, j, matrix1.getElement(i, j));
            }
            copyRow++;
        }
        timeLogger.end();
        Long id = Thread.currentThread().getId();
        System.out.println(String.format("Thread with id : %s execute time : %s", id.toString(), timeLogger.toString()));
    }
}
