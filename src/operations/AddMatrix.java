package operations;

import domain.Matrix;
import domain.SyncMatrix;
import operatos.Operator;
import util.TimeLogger;

public class AddMatrix<T> implements Runnable {

    private final SyncMatrix<T> syncMatrix1;
    private final SyncMatrix<T> syncMatrix2;
    private final SyncMatrix<T> result;
    private final Operator<T> addOp;
    private final int row;
    private final TimeLogger timeLogger = new TimeLogger();

    /**
     * @param syncMatrix1 first matrix
     * @param syncMatrix2 second matrix
     * @param result      the result matrix
     * @param addOp       op defines what sum between elements means (a+b)
     * @param lines       how many lines to update in the result matrix
     */
    public AddMatrix(SyncMatrix<T> syncMatrix1, SyncMatrix<T> syncMatrix2, SyncMatrix<T> result, Operator<T> addOp, int lines) {
        this.syncMatrix1 = syncMatrix1;
        this.syncMatrix2 = syncMatrix2;
        this.result = result;
        this.addOp = addOp;
        this.row = lines;
    }

    @Override
    public void run() {
        syncMatrix1.add(syncMatrix2, addOp);

        timeLogger.start();
        int copyRow = this.row;
        for (int i = 0; i < syncMatrix1.getN(); i++) {
            for (int j = 0; j < syncMatrix1.getM(); j++) {
                result.addElement(copyRow, j, syncMatrix1.getElement(i, j));
            }
            copyRow++;
        }
        timeLogger.end();
        Long id = Thread.currentThread().getId();
        System.out.println(String.format("Thread with id : %s execute time : %s", id.toString(), timeLogger.toString()));
    }
}
