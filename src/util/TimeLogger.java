package util;

public class TimeLogger {
    private Long startTime;
    private Long endTime;


    public void start() {
        startTime = System.nanoTime();
    }

    public void end() {
        endTime = System.nanoTime();
    }

    @Override
    public String toString() {
        return String.valueOf(String.format("%f milliseconds", (this.endTime - this.startTime) * 0.000001));
    }
}
