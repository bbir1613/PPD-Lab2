package util;

import domain.ParallelMatrix;
import domain.SyncMatrix;

import java.io.*;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class FileSystem {

    private static final String NUMBER_SEPARATOR = " ";
    private static final String NEW_LINE = System.lineSeparator();//java new line separator \n
    private final String filename;

    public FileSystem(String filename) {
        this.filename = filename;
    }

    public void writeToFile(int n, int m) {
        try {
            File file = new File(this.filename);
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    throw new RuntimeException("Couldn't create the file");
                }
            }
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            Random random = new Random();
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    //write to file
                    bufferedWriter.write(String.valueOf(ThreadLocalRandom.current().nextDouble(1,100)).substring(0,5));
                    bufferedWriter.write(NUMBER_SEPARATOR);
                    bufferedWriter.flush();
                }
                bufferedWriter.write(NEW_LINE);
                bufferedWriter.flush();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public <T> SyncMatrix<T> readFromFile(SyncMatrix<T> matrix, Map<T> map) {
        matrix = (SyncMatrix<T>) this.fileRead(matrix, map);
        return matrix;
    }

    public <T> ParallelMatrix<T> readFromFile(ParallelMatrix<T> matrix, Map<T> map) {
        matrix = (ParallelMatrix<T>) this.fileRead(matrix, map);
        return matrix;
    }

    private <T> Object fileRead(SyncMatrix<T> matrix, Map<T> map) {
        try {
            File file = new File(this.filename);

            FileReader fileReader = new FileReader(file.getAbsoluteFile());
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            for (int i = 0; i < matrix.getN(); i++) {
                //read one line from file
                line = bufferedReader.readLine();
                String[] lines = line.split(NUMBER_SEPARATOR);
                if (matrix.getM() > lines.length) {
                    throw new RuntimeException("Number of items lower than requested");
                }
                for (int j = 0; j < matrix.getM(); j++) {
                    matrix.addElement(i, j, map.map(lines[j]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matrix;
    }

    @FunctionalInterface
    public interface Map<T> {
        T map(String what);
    }
}
