package operatos;

public class MultiplyOperator extends AbsOperator<Double> {
    @Override
    public Double apply(Double x, Double y) {
        return x * y;
    }
}
