
package operatos;

/**
 * @param <T> Type of matrix
 *            <p>
 *            Used for defining custom operators for the type T like (+,-,*,/)
 */
@FunctionalInterface
public interface Operator<T> {
    T apply(T x, T y);
}
